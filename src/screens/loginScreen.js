import React , { Component } from 'react';
import {Button, View, TextInput, Modal, Text, Image,StyleSheet} from 'react-native';
import Auth0 from 'react-native-auth0';

class Auth extends Component {
  constructor() {
    super();
    this.state = {
      phone: '',
      code: '',
      codeRequestSent: false,
      LogginIn: false,
      isLoggedin: false,
      accessToken: '',
      idToken: '',
    };
    this.loginUser = this.loginUser.bind(this);
    this.getLoginCode = this.getLoginCode.bind(this);
    
  }

  componentDidMount() {
    this.auth0 = new Auth0({
      domain: 'dev-yox7uhij.us.auth0.com',
      clientId: 'R6lvuiBJnsfIAWT6457ATROTBGEM42aj',
    });
  }

  getLoginCode () {
    this.setState({LogginIn: true});
    this.auth0.auth
        .passwordlessWithSMS({
        phoneNumber: '+91'+this.state.phone,
        })
        .then(() => {
        this.setState({codeRequestSent: true});
        })
        .catch(console.error);
  }

  loginUser () {
    this.auth0.auth
    .loginWithSMS({
      phoneNumber: '+91'+this.state.phone,
      code: this.state.code,
    })
    .then(response => {
      console.log(response);
      this.setState({
        accessToken: response.accessToken,
        idToken: response.idToken,
        isLoggedin: true,
      });
    })
    .catch(console.error);
  }

  logout () {
    this.setState({
      phone: '',
      code: '',
      codeRequestSent: false,
      LogginIn: false,
      isLoggedin: false,
      accessToken: '',
      idToken: '',
    })
    console.log("Logged out")
  }
  render(){
    const {
        codeRequestSent, LogginIn, code, isLoggedin, accessToken, idToken,
      } = this.state;
      return (
        <View>
          {!codeRequestSent ? (
            <>
              <TextInput
                placeholder="Enter Phone"
                onChangeText={text => this.setState({phone: text})}
                keyboardType="number-pad"
                style={styles.textInput}
              />
              <Button
                title={LogginIn ? 'Processing...' : 'Get Code'}
                onPress={this.getLoginCode}

              />
            </>
          ) : (
            <>
              <TextInput
                placeholder="Enter Code"
                value={code}
                onChangeText={text => this.setState({code: text})}
                style={styles.textInput}
                keyboardType="number-pad"
              />
              <Button title="Login" onPress={this.loginUser} />
              <View>
                <Modal transparent={true} visible={isLoggedin}>
                  <View style={{backgroundColor: '#000000aa', flex: 1}}> 
                    <View style={{
                      backgroundColor: '#ffffff',
                      margin: 50,
                      padding: 40,
                      borderRadius: 10,
                    }}>
                      <Text> Login Successful 👍🏼🎉</Text>
                      <Text> Here are your details:</Text>
                      <Text> Access Token: {' ' + accessToken} {'\n'}</Text>
                      <Text>
                        Id Token:
                        {' ' + idToken.length > 25
                          ? `${idToken.substring(0, 25)}...`
                          : idToken}
                           {'\n'}
                      </Text>
                      <Button title="Logout"  onPress={() => this.logout()}/>
                    </View>
                  </View>
                </Modal>
              </View>
            </>
          )}
        </View>
      );
  }
}

const styles = StyleSheet.create({
    textInput : {
        height: 40, 
        width : 350,
        paddingHorizontal : 10,
        paddingVertical : 7,
        borderColor: 'gray', 
        borderRadius : 5,
        borderWidth: 1,
        fontSize : 20,
        marginBottom : 20
    }
});
export default Auth;
