/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
} from 'react-native';

import LoginScreen from './src/screens/loginScreen';
class App extends React.Component {
  render() {
    return (
      // <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <LoginScreen />
      </SafeAreaView>
    
    );
  }
};

export default App;
